# -*- coding:utf-8 -*-
"""provide the property list for app.py"""
from load_file import load_json
js_property = load_json()
#print(js_property)
property_list = []
for value1 in js_property.values():
    #print(value1)
    for value2 in value1['default_ps'].keys():
        if value2 not in property_list:
            property_list.append(value2)
print(property_list)


