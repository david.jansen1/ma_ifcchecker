# -*- coding:utf-8 -*-
import ifcopenshell
import json

def load_ifc():

    '''please input the ifc model need to be validated'''

    file = ifcopenshell.open('C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\Model\\IFC_ERC_main_building.ifc')

    return file


def load_json():
    """load correspondent json file (related to the source tool: Revit or Archicad and the version of the platforms) and converse it to dict"""
    file_json = open('C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\IFC_Validator\\template_Autodesk Revit 2021 (ENU).json', 'r')
    content = file_json.read()
    a = json.loads(content)
    return a

