The handover is divided into five parts:
1. The models (including the final version of revit and ifc)
2. Vortrag_powerpoint slides
3. Masterthesis_schriftfassung in pdf version
4. IFC Material Enrichment Tool (including pyrevit plugin and write to ifc python script)
5. IFC Validator Tool (including python script and flask app)

Introduction and other help:
1. How to integrate pyrevit script as revit plugin?
    - Youtube tutories (link: https://youtu.be/WJgmFkGHVPU)
2. Workflow of IFC Material Enrichment Tool: 
    - Use pyrevit plug in export a material list (json file) from the current revit doc
    - Use write to ifc python script to write the material list to ifc file
3. Revit export to IFC configuration?
    -import Revit_To_IFC Configuration.json in revit export IFC feature, please note the filepath of Revit_to_IFC_MappingFile.txt need to be replaced
4. How to run the flask app?
    -cd Terminal to IFC_Validator folder input 'flask run', before that should define the ifc file which needs to be checked in load_file.py
5. IFC Validator: change the importance factor of elements or properties.
    - in ifc_parse.py change the dict value of 'element_weight' and 'dic_weight_property' can be modified
